// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import com.deltaxml.merge.ThreeWayMerge;
import com.deltaxml.merge.ThreeWayMerge.ThreeWayResultPreset;
import java.io.File;

public class ThreeToTwoConflictingChanges {
  
  public static void main(String[] args) throws Exception {
    File dataFolder = new File("data/");
    File outputfolder = new File("output/");

    // Ancestor file
    File ancestorFile = new File(dataFolder, "base.xml");
    // Version files
    File edit1Version = new File(dataFolder, "edit1.xml");
    File edit2Version = new File(dataFolder, "edit2.xml");
    
    // Result File
    File resultFile = new File(outputfolder, "three-to-two-conflicting-changes-result.xml");

    ThreeWayMerge m= new ThreeWayMerge();
    m.setWordByWord(false);
    m.setResultPreset(ThreeWayResultPreset.CONFLICTING_CHANGES);
    m.merge(ancestorFile, edit1Version, edit2Version, resultFile);
  }
}
